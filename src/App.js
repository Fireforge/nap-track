import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
  Switch,
  Route
} from "react-router-dom";

import NapTrack from './components/NapTrack';
import LoadEvents from './components/LoadEvents';


function App() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  
  const theme = createTheme({
    palette: {
      type: prefersDarkMode ? 'dark' : 'light',
    },
  });
  
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Switch>
          <Route exact path="/">
            <NapTrack />
          </Route>
          <Route path="/load/:data">
            <LoadEvents />
          </Route>
        </Switch>
      </ThemeProvider>
    </div>
  );
}

export default App;
