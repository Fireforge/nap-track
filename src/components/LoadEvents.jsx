import React from 'react';
import {
    useParams,
    Redirect
} from "react-router-dom";

import ls from 'local-storage'

function LoadEvents() {
    let { data } = useParams();
    let events = JSON.parse(Buffer.from(data, 'base64'));
    ls.set('events', events);

    return (
        <Redirect to={{ pathname: "/" }}/>
    );
}

export default LoadEvents;