import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import { Box, Button } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider';
import Popover from '@material-ui/core/Popover';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import Snackbar from '@material-ui/core/Snackbar';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteIcon from '@material-ui/icons/Delete';

import { DateTime, Duration } from "luxon";

import ls from 'local-storage'

// TODO maybe this should be an enum of some type?
const categories = [
    "🔼 awake",
    "🔽 crib",
    "🔽 arms",
]

function filterObject(obj, callback) {
    return Object.fromEntries(Object.entries(obj).filter(([key, val]) => callback(val, key)));
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
        textAlign: 'center',
        alignContent: 'center',
    },
    title: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(1),
        margin: theme.spacing(1),
        // color: theme.palette.text.primary,
    },
    eventlist: {
        padding: theme.spacing(1),
    },
    eventbuttons: {
        padding: theme.spacing(1),
    }
}));

function NapTrack() {
    // Reconstitute the datetimes in local storage into Luxon DateTimes. For some reason they get 
    //   converted to ISO strings when we save them to storage, so we have to convert them back.
    var ls_events = (ls.get('events') || []).map(({ datetime, category }) => ({ datetime: DateTime.fromISO(datetime), category }));

    const [events, setEvents] = useState(ls_events);
    // Convenience function to keep the local storage up-to-date with the events state
    const setEventsLS = (events) => {
        setEvents(events);
        ls.set('events', events);
    }

    const AddNapEvent = (event) => {
        setEventsLS([...events, event]);
    };

    const ClearEvents = () => {
        setEventsLS([]);
    }

    const [tabValue, setTabValue] = React.useState(0);
    const handleTabChange = (_, newValue) => {
        setTabValue(newValue);
    };

    const handleDelete = (event) => {
        setEventsLS(events.filter(item => item !== event))
    };

    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const handleCopyLoadLink = (event) => {
        // create the link that contains the current ls_events encoded in it
        let link = window.location.href + "load/" + Buffer.from(JSON.stringify(ls_events)).toString('base64');
        // copy the link into the clipboard
        navigator.clipboard.writeText(link);
        // display the "link copied" snackbar
        setSnackbarOpen(true);
    };
    const handleSnackbarClose = (event) => {
        setSnackbarOpen(false);
    };

    let fixed_height = 275;
    const classes = useStyles();
    return (
        <Box id="NapTrack" className={classes.root} display="flex" flexDirection="column">
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Nap Track
                    </Typography>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={handleCopyLoadLink}>
                        <FileCopyIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <NapEventList flexGrow={1} overflow="auto" events={events} onDelete={handleDelete}/>
            <Box maxHeight={fixed_height} minHeight={fixed_height}>
                <Paper square>
                    <Tabs
                        value={tabValue}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={handleTabChange}
                        variant="fullWidth"
                    >
                        <Tab label="Add Events" />
                        <Tab label="Stats" />
                    </Tabs>
                </Paper>
                <TabPanel value={tabValue} index={0}>
                    <NapEventButtonGrid addNapEvent={AddNapEvent} clearEvents={ClearEvents} />
                </TabPanel>
                <TabPanel value={tabValue} index={1}>
                    <NapStats events={events} />
                </TabPanel>
            </Box>
            <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={snackbarOpen}
                onClose={handleSnackbarClose}
                message="Link copied to clipboard!"
            />
        </Box>
    );
}

function NapEventList(props) {
    const classes = useStyles();
    return (
        <Box id="NapEventList" className={classes.eventlist} {...filterObject(props, (_, k) => k !== 'onDelete')}>
            {props.events.map((event, i) => (
                <NapEvent event={event} key={i} onDelete={props.onDelete}/>
            ))}
        </Box>
    );
}

function NapEvent(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        setAnchorEl(null);
    };

    const displayEvent = (e) => {
        var time = e.datetime.toLocaleString({ hour: 'numeric', minute: 'numeric', second: 'numeric' });
        return time + ": " + e.category;
    };
    const handleDelete = () => {
        props.onDelete(props.event);
        handleClose();
    };

    return (
        <Box>
            <Paper className={classes.paper} onClick={handleClick}>{displayEvent(props.event)}</Paper>
            <Popover
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
                BackdropProps={{
                    invisible: false
                }}
                >
                <Paper className={classes.paper}>{displayEvent(props.event)}</Paper>
                <List component="nav" aria-label="main mailbox folders">
                    <ListItem button onClick={handleDelete}>
                        <ListItemIcon>
                            <DeleteIcon color="secondary"/>
                        </ListItemIcon>
                        <ListItemText primary="delete" primaryTypographyProps={{variant: "button", color: "secondary"}}/>
                    </ListItem>
                </List>
            </Popover>
        </Box>
    );
}

function NapStats(props) {
    const classes = useStyles();

    // For each category, filter out each event for that category from the event list, then map over slices 
    //   of the datetime objects to get durations, then reduce the durations to sum totals
    let map1 = new Map();
    for (const c of categories) {
        map1.set(c, [Duration.fromMillis(0)])
    }
    // console.log(map1)
    for (const i of [...Array(props.events.slice(0, -1).length).keys()]) {
        let [start, end] = props.events.slice(i, i + 2)
        map1.get(start.category).push(end.datetime.diff(start.datetime))
    }
    // console.log(map1)
    let map2 = new Map();
    for (const c of categories) {
        var dur_sum = map1.get(c).reduce((acc, cur) => acc.plus(cur))
        map2.set(c, dur_sum)
    }
    // console.log(map2)
    let down_total = map2.get(categories[1]).plus(map2.get(categories[2]));
    const duration_format = "h:mm:ss";
    const displayCategoryTotal = (i) => {
        return categories[i] + ' - ' + map2.get(categories[i]).toFormat(duration_format)
    }

    return (
        <Box id="NapStats" className={classes.stats} {...props}>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        {displayCategoryTotal(0)}
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        {displayCategoryTotal(1)}
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        {displayCategoryTotal(2)}
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper variant="outlined" className={classes.paper}>
                        {'🔽 TOTAL - ' + down_total.toFormat(duration_format)}
                    </Paper>
                </Grid>
            </Grid>
        </Box>
    );
}

function NapEventButtonGrid(props) {
    const classes = useStyles();
    return (
        <Box id="NapEventButtonGrid" className={classes.eventbuttons}>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <AddNapEventButton name={categories[0]} onClick={props.addNapEvent} />
                </Grid>
                <Grid item xs={6}>
                    <AddNapEventButton name={categories[1]} onClick={props.addNapEvent} />
                </Grid>
                <Grid item xs={6}>
                    <AddNapEventButton name={categories[2]} onClick={props.addNapEvent} />
                </Grid>
                <Grid item xs={12}>
                    <Divider variant="middle"/>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" id="ClearEventsButton" onClick={props.clearEvents} startIcon={<DeleteIcon />}>Clear</Button>
                </Grid>
            </Grid>
        </Box>
    );
}

function AddNapEventButton(props) {
    return (
        <Button variant="contained" color="primary" size="large" onClick={() => props.onClick({ datetime: DateTime.now(), category: props.name })}>
            {props.name}
        </Button>
    );
}

export default NapTrack;